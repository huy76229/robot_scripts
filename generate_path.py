"""
Usage:
    $ python generate_path.py --input ./songpa_1_path.txt --dist 5

Output:
    path_data.json file

"""

import argparse
import sys
import os
import json
from datetime import datetime
import numpy as np


CREATE_REVERSE_PATH_AND_CONCAT = True

NEW_PATH_VERSION = False

if NEW_PATH_VERSION:
    MAP_NAME = "mapId"
    PATROL_PATH_NAME = "patrolPath"
    PATROL_DISINFECTION_PATH_NAME = "disinfectionPath"
    PATH_ID = "pathId"
    PATH_TYPE = "path_type"
    PATH_POINT = "points"
    PATH_LOOP = "loop"
    PATH_OPEN = "open"

else:
    MAP_NAME = "map_name"
    PATROL_PATH_NAME = "patrol_path"
    PATROL_DISINFECTION_PATH_NAME = "disinfection_path"
    PATH_ID = "path_id"
    PATH_TYPE = "path_type"
    PATH_POINT = "points"
    PATH_LOOP = "loop"
    PATH_OPEN = "open"


def create_new_file():
    # create new folder
    output_file_folder_ = "./{}".format(datetime.now().strftime('%Y%m%d%H%M%S'))
    os.makedirs(output_file_folder_)
    output_path_file = "{}/path_data.json".format(output_file_folder_)

    return output_path_file


def open_json_file(path):
    with open(path) as json_file:
        data = json.load(json_file)
        return data


def save_json_file(file_path, data):
    # encode data in json format
    loaded_data = json.dumps(data, indent=4, sort_keys=True)

    # write data
    with open(file_path, 'w') as json_file:
        json_file.write(loaded_data)


def add_point_to_path_file(file_path, points):

    data = dict()
    data[MAP_NAME] = ""
    data[PATROL_PATH_NAME] = []
    data[PATROL_DISINFECTION_PATH_NAME] = []

    path_id_ = datetime.now().strftime('%Y%m%d%H%M%S')
    path_id_ = path_id_[-6:]

    path_group = dict()
    path_group[PATH_ID] = "P{}".format(path_id_)
    path_group[PATH_TYPE] = PATH_OPEN
    path_group[PATH_POINT] = []

    rot_angle = 0.0
    for idx, point in enumerate(points):
        if idx + 1 < len(points):
            current_point = point
            next_point = points[idx+1]
            rot_angle = round(np.arctan2(next_point[1] - current_point[1], next_point[0] - current_point[0]), 3)

        rot_angle = round((rot_angle - 6.284), 3) if rot_angle >= 3.142 else rot_angle

        point_id_ = "D{}_{}".format(path_id_, idx)

        if NEW_PATH_VERSION:
            point_data = {
                "pointId": point_id_,
                "pointName": "",
                "pointType": "",
                "mapId": "",
                "pose": {
                    "x": point[0],
                    "y": point[1],
                    "theta": rot_angle
                },
                "attribute": []
            }

        else:
            point_data = {
                "type": point_id_,
                "x": point[0],
                "y": point[1],
                "theta": rot_angle
            }

        path_group[PATH_POINT].append(point_data)

    if CREATE_REVERSE_PATH_AND_CONCAT:
        start_idx = idx+1
        points_ = list(reversed(points))
        del points_[0]

        for idx, point in enumerate(points_):
            if idx + 1 < len(points_):
                current_point = point
                next_point = points_[idx + 1]
                rot_angle = round(np.arctan2(next_point[1] - current_point[1], next_point[0] - current_point[0]), 3)

            rot_angle = round((rot_angle - 6.284), 3) if rot_angle >= 3.142 else rot_angle

            point_id_ = "D{}_{}".format(path_id_, start_idx)
            start_idx += 1

            if NEW_PATH_VERSION:
                point_data = {
                    "pointId": point_id_,
                    "pointName": "",
                    "pointType": "",
                    "mapId": "",
                    "pose": {
                        "x": point[0],
                        "y": point[1],
                        "theta": rot_angle
                    },
                    "attribute": []
                }

            else:
                point_data = {
                    "type": point_id_,
                    "x": point[0],
                    "y": point[1],
                    "theta": rot_angle
                }

            path_group[PATH_POINT].append(point_data)

    data[PATROL_PATH_NAME].append(path_group)

    save_json_file(file_path, data)

    print("[DONE] Path file({} points) conversion is complete : {}".format(len(path_group[PATH_POINT]), file_path))


def p2p_dist(point1, point2):
    x_dist = float(point1[0]) - float(point2[0])
    y_dist = float(point1[1]) - float(point2[1])
    dist_ = np.sqrt(x_dist * x_dist + y_dist * y_dist)

    if dist_ >= point2point_dist:
        return True

    return False


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Convert Path")
    parser.add_argument('--input', type=str, default="./songpa_1_path.txt", help='path.txt file directory',
                        required=True)
    parser.add_argument('--dist', type=float, default=5, help='point to point distance(meter)',
                        required=True)
    opt = parser.parse_args()

    input_file_path = opt.input
    point2point_dist = opt.dist

    # check file path
    if os.path.exists(input_file_path):
        print("Path File path : " + input_file_path)

        points_list = []
        start_point = []
        cnt = 0

        # read path data file
        file = open("{}".format(input_file_path), "r")
        while True:
            cnt += 1
            line = file.readline().split('\n')[0]
            if not line:
                break

            points_ = line.split(',')

            # add start point
            if cnt == 1:
                points_list.append([float(points_[0]), float(points_[1])])
                start_point = points_

            if len(points_) >= 2:
                if -6899 <= float(points_[0]) <= 6899 and -3412 <= float(points_[1]) <= 3412:
                    if p2p_dist(start_point, points_):
                        points_list.append([float(points_[0]), float(points_[1])])
                        start_point = points_

        file.close()

        # create file
        output_file_ = create_new_file()

        # add point to path file
        add_point_to_path_file(output_file_, points_list)

    else:
        print("Can not find the path file : " + input_file_path)

