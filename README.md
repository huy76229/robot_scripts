# Convert Recorded Path to Planner Path
Converts the path created by the move base flex into a path usable by the planner.
## Required Package
* python3
* numpy
## Install package
```
pip install numpy
```
## Usage
```
python generate_path.py --input {path file(.txt) directory} --dist {point_to_point_distance(meter)}
```
## Example
```
python generate_path.py --input ./songpa_1_path.txt --dist 5
```
